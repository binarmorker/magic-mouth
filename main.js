#!/usr/bin/env node

const { parseArgs } = require('node:util')
const { spawn } = require('node:child_process')
const which = require('which')
const prompt = require('prompt-sync')({ sigint: true })
const { Client, PermissionsBitField, IntentsBitField } = require('discord.js')
const { VoiceConnectionStatus, joinVoiceChannel, entersState, createAudioPlayer, createAudioResource } = require('@discordjs/voice')
const { streamingMessage, noConnectionMessage, noInstallMessage, noVariableMessage, getProgressMessage } = require('./messages')
const { getEnvValue, setEnvValue } = require('./env')
const getPatchedConnection = require('./patch')

console.log(`• ▌ ▄ ·.  ▄▄▄·  ▄▄ • ▪   ▄▄·     • ▌ ▄ ·.       ▄• ▄▌▄▄▄▄▄ ▄ .▄
·██ ▐███▪▐█ ▀█ ▐█ ▀ ▪██ ▐█ ▌▪    ·██ ▐███▪▪     █▪██▌•██  ██▪▐█
▐█ ▌▐▌▐█·▄█▀▀█ ▄█ ▀█▄▐█·██ ▄▄    ▐█ ▌▐▌▐█· ▄█▀▄ █▌▐█▌ ▐█.▪██▀▐█
██ ██▌▐█▌▐█ ▪▐▌▐█▄▪▐█▐█▌▐███▌    ██ ██▌▐█▌▐█▌.▐▌▐█▄█▌ ▐█▌·██▌▐▀
▀▀  █▪▀▀▀ ▀  ▀ ·▀▀▀▀ ▀▀▀·▀▀▀     ▀▀  █▪▀▀▀ ▀█▄▀▪ ▀▀▀  ▀▀▀ ▀▀▀ ·

*** Stream Audio Through Discord ***             CTRL+C to quit
`)

// Config
const args = parseArgs({
  options: {
    BOT_TOKEN: {
      type: 'string',
      default: getEnvValue('BOT_TOKEN') || ''
    },
    CLIENT_ID: {
      type: 'string',
      default: getEnvValue('CLIENT_ID') || ''
    },
    USER_ID: {
      type: 'string',
      default: getEnvValue('USER_ID') || ''
    }
  },
  allowPositionals: true
})
const { BOT_TOKEN, CLIENT_ID, USER_ID } = args.values
const [SELECTED_INPUT] = args.positionals
setEnvValue('BOT_TOKEN', BOT_TOKEN)
setEnvValue('CLIENT_ID', CLIENT_ID)
setEnvValue('USER_ID', USER_ID)

// Globals
let connection = null
let player = null
let resource = null
let stream = null

// Disconnect bot
async function cleanupBot({ cleanup = true, exit = false, error = null, promise = null }) {
  if (cleanup) {
    await connection?.disconnect()
    await connection?.destroy()
    connection = null
    player?.stop()
    player?.removeAllListeners()
    player = null
    resource = null
    stream?.removeAllListeners()
    stream?.kill()
    stream = null
  }

  if (error && promise) {
    console.error(error, 'Unhandled Rejection at Promise', promise)
    process.exit(1)
  } else if (error) {
    console.error(error, 'Uncaught Exception thrown')
    process.exit(1)
  }

  if (exit) {
    process.exit(0)
  }
}

// Obtain the current voice channel
function getVoiceChannel() {
  const channels = client.channels.cache.values()

  for (const channel of channels) {
    if (!channel.isVoiceBased()) continue

    for (const member of channel.members.values()) {
      if (member.user.id === USER_ID) {
        return channel
      }
    }
  }
}

// Print over a line
function printProgress(progress) {
  const message = streamingMessage()
  const lines = message.split(/\r\n|\r|\n/)
  const width = lines[0].length
  process.stdout.moveCursor(-width, -5)
  process.stdout.clearScreenDown()
  process.stdout.write(message.replace(/│\s*LOADING\.\.\.\s*│/, getProgressMessage(progress.replace('\r', '').trim(), width)))
}

// Bot processing
async function startBot() {
  const voice = getVoiceChannel()
  
  if (!voice || !voice.joinable) {
    console.log(noConnectionMessage(CLIENT_ID, permissions))
    process.exit(1)
  }

  let list = await new Promise((resolve) => {
    const fmediaList = spawn('fmedia', ['--list-dev'])
    fmediaList.stdout.on('data', data => resolve(data.toString()))
  })
  list = [
    '0: All desktop audio',
    ...list.substring(19, list.indexOf('Capture:\n')).split('device #').slice(1).map(x => x.replace('\n', ' /').replace('\n', ''))
  ]
  let loopback
  
  if (SELECTED_INPUT !== undefined && parseInt(SELECTED_INPUT, 10) && +SELECTED_INPUT > 0 && +SELECTED_INPUT < list.length) {
    loopback = +SELECTED_INPUT
  } else if (SELECTED_INPUT !== undefined) {
    if (SELECTED_INPUT === 'all') loopback = 0
    else {
      const found = list.findIndex(x => x.includes(SELECTED_INPUT))
      if (found > 0) loopback = found
    }
  } else {
    console.log(list.join('\n'))
    loopback = prompt('What input do you want to use? (Write the number, then press Enter) ', '0')
  }

  player = createAudioPlayer()
  connection = joinVoiceChannel({ adapterCreator: voice.guild.voiceAdapterCreator, guildId: voice.guild.id, channelId: voice.id })
  connection = getPatchedConnection(connection) // Patch to avoid autodisconnects
  connection.subscribe(player)
  
  const createStream = () => {
    if (stream) {
      process.stdout.moveCursor(0, -5)
    }

    stream?.stdout?.removeAllListeners()
    stream?.stderr?.removeAllListeners()
    stream?.removeAllListeners()
    stream?.kill()
    setTimeout(() => {
      stream = spawn('fmedia', ['--record', `--dev-loopback=${loopback}`, '-o', '@stdout.wav'])
      resource = createAudioResource(stream.stdout)
      console.log(streamingMessage())
      process.stdout.moveCursor(0, -1)
      player.play(resource)
      player.on('error', createStream)
      stream.stdout.on('error', createStream)
                   .on('close', createStream)
                   .on('exit', createStream)
      stream.stderr.on('error', createStream)
                   .on('close', createStream)
                   .on('exit', createStream)
                   .on('data', d =>  /^φfmedia|^Recording/.test(d.toString()) ? null : printProgress(d.toString()))
      stream.on('error', createStream)
            .on('close', createStream)
            .on('exit', createStream)
    }, 500)
  }
  createStream()
  await entersState(connection, VoiceConnectionStatus.Ready, 30_000)

  setInterval(() => {
    if (connection) {
      const channel = client.channels.cache.get(connection.joinConfig.channelId)

      if (channel?.members?.reduce((s, member) => s + (member.user.bot ? 0 : 1), 0) <= 0) {
        cleanupBot({ exit: true })
      }
    }
  }, 1000)
}

// Cleanup on exit
process.on('exit', cleanupBot.bind(null, { cleanup: true }))
process.on('SIGINT', cleanupBot.bind(null, { exit: true })) // catches ctrl+c event
process.on('SIGUSR1', cleanupBot.bind(null, { exit: true })) // catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR2', cleanupBot.bind(null, { exit: true }))
process.on('uncaughtException', error => cleanupBot({ exit: true, error })) // catches uncaught exceptions
process.on('unhandledRejection', (error, promise) => cleanupBot({ exit: true, error, promise })) // catches uncaught exceptions

if (!BOT_TOKEN || !CLIENT_ID || !USER_ID) {
  console.log(noVariableMessage())
  process.exit(1)
}

if (!which.sync('fmedia', { nothrow: true })) {
  console.log(noInstallMessage())
  process.exit(1)
}

// Bot stuff
const permissions = new PermissionsBitField()
permissions.add(PermissionsBitField.Flags.ViewChannel)
permissions.add(PermissionsBitField.Flags.SendMessages)
permissions.add(PermissionsBitField.Flags.EmbedLinks)
permissions.add(PermissionsBitField.Flags.Connect)
permissions.add(PermissionsBitField.Flags.Speak)
permissions.add(PermissionsBitField.Flags.UseVAD)

const intents = new IntentsBitField()
intents.add(IntentsBitField.Flags.Guilds)
intents.add(IntentsBitField.Flags.GuildVoiceStates)

const client = new Client({ intents })
client.on('ready', startBot)
client.login(BOT_TOKEN)

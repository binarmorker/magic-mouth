const fs = require('node:fs')
const os = require('node:os')
const path = require('node:path')
const ospath = require('ospath')

const envFilePath = path.resolve(ospath.data(), './.magicmouth')
const readEnvVars = () => {
  try {
    return fs.readFileSync(envFilePath, { encoding: 'utf-8', flag: 'r' }).split(os.EOL)
  } catch (_) {
    return []
  }
}

const getEnvValue = key => {
  const matchedLine = readEnvVars().find(line => line.split('=')[0] === key)
  return matchedLine ? matchedLine.split('=')[1] : null
}

const setEnvValue = (key, value) => {
  if (!value) return

  const envVars = readEnvVars()
  const targetLine = envVars.find(line => line.split('=')[0] === key)

  if (targetLine) {
    const targetLineIndex = envVars.indexOf(targetLine)
    envVars.splice(targetLineIndex, 1, `${key}=${value}`)
  } else {
    envVars.push(`${key}=${value}`)
  }

  fs.writeFileSync(envFilePath, envVars.join(os.EOL), { encoding: 'utf-8', flag: 'w', mode: 0o644 })
}

module.exports = {
  getEnvValue,
  setEnvValue
}

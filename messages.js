const boxen = require('boxen')
const chalk = require('chalk')
const ospath = require('ospath')

module.exports = {
  streamingMessage: () => boxen(`
${chalk.green('Streaming desktop audio!')}
                    LOADING...                         
    `,
    { padding: { left: 3, right: 3 }, textAlignment: 'center' }
  ),
  getProgressMessage: (message, width) => {
    const half = (width - message.length) / 2
    const left = Math.floor(half) - 1
    const right = Math.ceil(half) - 1
    const str = (left >= 0 && right >= 0)
      ? `│${' '.repeat(left)}${message}${' '.repeat(right)}│`
      : `│${' '.repeat(width - 2)}│`
    return str
  },
  noConnectionMessage: (CLIENT_ID, permissions) => boxen(`
You are not in a voice chat or I do not have permission to connect to it.
If you have not invited me to your Discord server yet, follow this link:

${chalk.green(`https://discord.com/oauth2/authorize?client_id=${CLIENT_ID}&scope=bot&permissions=${permissions.bitfield}`)}
    `,
    { padding: { left: 3, right: 3 }, textAlignment: 'center', title: 'Error', titleAlignment: 'center' }
  ),
  noInstallMessage: () => boxen(`
fmedia is not installed or has not been added to your PATH variable
Please install fmedia using the following link:
    
${chalk.green('https://www.videohelp.com/software/fmedia')}
    `,
    { padding: { left: 3, right: 3 }, textAlignment: 'center', title: 'Error', titleAlignment: 'center' }
  ),
  noVariableMessage: () => boxen(`
The environment variables are not set properly, preventing the bot from running.
Ensure the following variables are correctly set:
    
BOT_TOKEN: The secret token for the Discord bot you created
CLIENT_ID: The OAuth Client ID for the invitation link
USER_ID: Your Discord user's ID for the bot to join along
    
All of these may be set in a file named ".magicmouth" in your local config directory (${ospath.data()}).
Alternatively, you may pass those variables as arguments like so: --BOT_TOKEN xx
    `,
    { padding: { left: 3, right: 3 }, textAlignment: 'center', title: 'Error', titleAlignment: 'center' }
  )
}
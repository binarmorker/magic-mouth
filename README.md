magicmouth
==========
*2nd level illusion (ritual)*

You implant a **Stream** within a **Discord bot** in range, a **Stream** that is uttered when a **command** is **called**.

***

Using the power of your own custom Discord bot, create a stream of audio coming from your default audio output and forward it through the bot's voice.

Installation
------------
Easily enough, the bot itself only needs to be installed using `npm i -g magicmouth`.

The only external dependency is the fmedia software: https://www.videohelp.com/software/fmedia

However, the setup to run the bot is a bit more complicated.

Usage
-----
First, you must create a Discord bot using the following guide: https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot
Then, once your bot is created, you may copy its token and its oauth client id into a file document. Afterwards, you will need to get your own Discord user's ID by opening Discord, going to the User Settings, Advanced, and enabling Developer Mode, then by going in User Settings, My Account, and clicking Copy ID under the three dots next to your username.

Once you have all of those, open a command-line and type the following:

```
magicmouth --USER_ID=<your_discord_user_id> --CLIENT_ID=<your_oauth_client_id> --BOT_TOKEN=<your_bot_token>
```

The bot should start normally and tell you it needs to be invited to a server, given the correct permissions to connect to a voice channel, then connect to that voice channel. Starting the bot again with the same command once you're connected should connect the bot along with you.

Once you started the bot correctly once, you may from thereon call the bot using only the command `magicmouth` without parameters. The first command simply writes in the variables to the configuration. If you ever want to change a variable, pass the correct parameter again. You may also pass `all` or the index or name of the audio input you want to choose, like so: `magicmouth 2`.

Voilà! You are now streaming your desktop audio from a Discord bot! Go find your favorite soundboard app and have fun!

Why this exists
---------------
As a Dungeons & Dragons Dungeon Master, I need to give music and audio effects to my players to make them feel immersed in the numerous encounters and sceneries they come across. However, since going fully online since the start of the pandemic, I cannot remember one time I liked how a Discord audio bot was working, as I usually like to splice my audio using software like Audacity and soundboards. I also prefer having full control over the volume to give a fully immersive experience to my players, else they mute the bot for being too loud or disruptive, and I have lost this part of the immersion for some.

I have therefore created this Discord bot to alleviate all of my issues and go back to the way I used to do my sessions when in-person: using my own speakers as audio output, and doing everything on my personal computer.

This bot serves this purpose perfectly, allowing me to even mute the bot on Discord for myself while I have all the audio playing at a comfortable level in my headset speakers.

Issues
------
I will not support unorthodox setups, as this is only a personal project I do on my free time. If you encounter any issues with ffmpeg, try to install it manually. If you encounter difficulties setting up the Discord bot, please refer to tutorials for how to create Discord bot. If you encounter any issue not described, feel free to open a ticket on GitLab. If you want something implemented into the bot, or want to fix an issue with your unique setup, merge requests are welcome.

Thanks
------
- Credit to the entire Discord team for their bot creation process, very cool and easy.
- Huge thanks to the team behind DiscordJS, making it easy to create a Discord bot from scratch.
- Many thanks to ffmpeg for being the go-to utility for audio and video processing.
- Huge credits to fmedia, the only external dependency of this project, for making loopback audio streaming possible.
- All other package maintainers I cannot possibly begin to enumerate.
- All my friends, whom I broke the eardrums trying to encode the audio correctly.